# CuckooSandboxAnalyzer

Program that use machine learning to determine a program malicious or not by using reports generated in Cuckoo Sandbox 

# Prepare to run
Start VM with cuckoo

## Determine cuckoo api tocken
Open terminal and execute:
```cat .cuckoo/conf/cuckoo.conf | grep api_token```
And you get API token that is need to change in code of main.py

## Run cuckoo
Run VirtualBox program.
Run VirtualBox virtual machine cuckoo and wait 30 seconds.
Open terminal and type: `cuckoo`, the cuckoo server might be strated successfully.

## Run cuckoo API server
Open another tab of terminal and type: `cuckoo api --host 0.0.0.0 --port 8090`.
