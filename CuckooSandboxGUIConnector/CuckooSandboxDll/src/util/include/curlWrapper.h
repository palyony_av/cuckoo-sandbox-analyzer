#ifndef CURL_WRAPPER_H
#define CURL_WRAPPER_H

void* initCurl();
void uninitCurl(void** pInCurl);
long uploadFile(void* pInCurl, const char* szFilepath, const char* szNetAddress);
/*
 * Send get request and return HTTP error code
 * If success write status received from server
 */
long sendGetRequest(void* pInCurl, const char* szNetAddress, int* pServerResponseStatus);

#endif // !CURL_WRAPPER_H
