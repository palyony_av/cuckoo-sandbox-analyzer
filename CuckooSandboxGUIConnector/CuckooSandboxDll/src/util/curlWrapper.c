#include <stdio.h>
#include <sys/stat.h>

#define CURL_STATICLIB
#include "include/curl/curl.h"
#include "include/curlWrapper.h"

#pragma comment(lib, "libcurl_a.lib" )
#pragma comment(lib, "wldap32.lib" )
#pragma comment(lib, "crypt32.lib" )
#pragma comment(lib, "Ws2_32.lib" )
#pragma comment(lib, "Normaliz.lib")

/* used in conjunction with uploadFile */
static size_t read_callback(void* pOutBuf, size_t size, size_t nmemb, void* stream);
/* used in conjunction with sendGetRequest */
static size_t write_callback(void* pInBuf, size_t size, size_t nmemb, void* pUsrBuf);

void* initCurl()
{
    CURL* pCurl = NULL;

    /* In windows, this will init the winsock stuff */
    curl_global_init(CURL_GLOBAL_ALL);
    /* get a curl handle */
    pCurl = curl_easy_init();
    if (pCurl == NULL)
    {
        /* always cleanup */
        curl_easy_cleanup(pCurl);
        curl_global_cleanup();
        return NULL;
    } else {
        return pCurl;
    }
}

void uninitCurl(void ** pInCurl)
{
    CURL* pCurl = (CURL*)*pInCurl;

    if (pCurl != NULL)
    {
        /* always cleanup */
        curl_easy_cleanup(pCurl);
        curl_global_cleanup();
        *pInCurl = NULL;
    }
}

long uploadFile(void * pInCurl, const char * szFilepath, const char * szNetAddress)
{
    CURL* pCurl = (CURL*)pInCurl;
    CURLcode retCode = CURLE_OK;
    struct stat file_info;
    FILE* fd;
    long httpErrorCode = 0;
    errno_t errorNoCode;

    /* open file to upload */
    errorNoCode = fopen_s(&fd, szFilepath, "rb");
    if (errorNoCode != 0)
    {
        goto Exit;
    }

    /* to get the file size */
    if (fstat(_fileno(fd), &file_info) != 0)
    {
        goto Exit;
    }

    if (pCurl == NULL)
    {
        goto Exit;
    }

    /* reset all options of a libcurl session handle */
    curl_easy_reset(pInCurl);

    /* we want to use our own read function */
    curl_easy_setopt(pCurl, CURLOPT_READFUNCTION, read_callback);

    /* enable uploading */
    curl_easy_setopt(pCurl, CURLOPT_UPLOAD, 1L);

    /* specify target */
    curl_easy_setopt(pCurl, CURLOPT_URL, szNetAddress);

    /* now specify which pointer to pass to our callback */
    curl_easy_setopt(pCurl, CURLOPT_READDATA, fd);

    /* Set the size of the file to upload */
    curl_easy_setopt(pCurl, CURLOPT_INFILESIZE_LARGE, (curl_off_t)file_info.st_size);

    /* Now run off and do what you've been told! */
    retCode = curl_easy_perform(pCurl);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }

    retCode = curl_easy_getinfo(pCurl, CURLINFO_RESPONSE_CODE, &httpErrorCode);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }

Exit:
    if (retCode != CURLE_OK || pCurl == NULL || errorNoCode != 0)
    {
        httpErrorCode = 0;
    }

    if (fd != NULL)
    {
        fclose(fd);
        fd = NULL;
    }

    return httpErrorCode;
}

long sendGetRequest(void* pInCurl, const char* szNetAddress, int* pServerResponseStatus)
{
    CURL* pCurl = (CURL*)pInCurl;
    CURLcode retCode = CURLE_OK;
    long httpErrorCode;
    curl_off_t downloadSize;

    if (pCurl == NULL)
    {
        goto Exit;
    }

    /* reset all options of a libcurl session handle */
    curl_easy_reset(pInCurl);

    /* specify target */
    retCode = curl_easy_setopt(pCurl, CURLOPT_URL, szNetAddress);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }
    curl_easy_setopt(pCurl, CURLOPT_WRITEFUNCTION, write_callback);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }

    if (pServerResponseStatus != NULL)
    {
        curl_easy_setopt(pCurl, CURLOPT_WRITEDATA, pServerResponseStatus);
        if (retCode != CURLE_OK)
        {
            goto Exit;
        }
    }

    /* Now run off and do what you've been told! */
    retCode = curl_easy_perform(pCurl);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }

    retCode = curl_easy_getinfo(pCurl, CURLINFO_RESPONSE_CODE, &httpErrorCode);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }

    retCode = curl_easy_getinfo(pCurl, CURLINFO_SIZE_DOWNLOAD_T, &downloadSize);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }

Exit:
    if (retCode != CURLE_OK || pCurl == NULL)
    {
        /* Return unknown 0 code */
        return 0;
    }
    return httpErrorCode;
}

/*
 * ==============================
 * Callback section
 * ==============================
 */

/* used in conjunction with uploadFile */
size_t read_callback(void* pOutBuf, size_t size, size_t nmemb, void* stream)
{
    size_t retcode;
    curl_off_t nread;

    /* in real-world cases, this would probably get this data differently
       as this fread() stuff is exactly what the library already would do
       by default internally */

    retcode = fread(pOutBuf, size, nmemb, stream);

    nread = (curl_off_t)retcode;

    return retcode;
}

/* used in conjunction with sendGetRequest */
size_t write_callback(void* pInBuf, size_t size, size_t nmemb, void* pUsrBuf)
{
    size_t totalSize = size * nmemb;

    if (totalSize > sizeof(int))
    {
        int* serverState = (int*)pUsrBuf;
        *serverState = 0xFFFFFFFF;
        return size * nmemb;
    }
    else {
        memcpy(pUsrBuf, pInBuf, totalSize);

        return totalSize;
    }
}
