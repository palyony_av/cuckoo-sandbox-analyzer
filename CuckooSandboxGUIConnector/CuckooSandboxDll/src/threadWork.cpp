#include <Windows.h>
#include <cstdint>
#include <chrono>

#include "include/sandboxAnalyzerContext.h"

#include "include/threadWork.h"

#define MAX_KEY_LENGTH 255
#define AV_SUBKEY_PATH "path"
#define AV_SUBKEY_STATE "state"
#define AV_SUBKEY_ANSWER "answer"

using namespace std::chrono_literals;

enum class SandboxAnalyzerState
{
    waitForAnalysis = 0,
    analyseIsRunning = 1,
    analyseIsEnded = 2,
    analyseIsFailed = 3
};

bool setAnalyseStateAndAnsw(HKEY hParentKey, const char* pKeyName, SandboxAnalyzerState state, int answ)
{
    bool result = false;
    LSTATUS retCode = 0;
    HKEY hCurrentKey = nullptr;
    uint32_t status = static_cast<uint32_t>(state);

    retCode = RegCreateKeyExA(
        hParentKey,                           // Handle of the parent registry key.
        pKeyName,                              // Name of the new key to open or create.
        0,                                    // Reserved, pass 0.
        nullptr,                              // The user-defined class type of this key.
        0,                                    // Flags controlling the key creation
        KEY_ALL_ACCESS,                       // Access level desired.
        nullptr,                              // Security attributes for the key.
        &hCurrentKey,                         // Destination for the resulting handle.
        nullptr                               // Receives REG_CREATED_NEW_KEY or REG_OPENED_EXISTING_KEY.
    );
    if (retCode != ERROR_SUCCESS)
    {
        result = false;
        goto Exit;
    }

    /* Set status */
    retCode = RegSetValueExA(hCurrentKey, AV_SUBKEY_STATE, 0, REG_DWORD,
        reinterpret_cast<BYTE*>(&status), sizeof(uint32_t));
    if (retCode != ERROR_SUCCESS)
    {
        result = false;
        goto Exit;
    }

    if (state == SandboxAnalyzerState::analyseIsEnded)
    {
        /* Set answer */
        retCode = RegSetValueExA(hCurrentKey, AV_SUBKEY_ANSWER, 0, REG_DWORD,
            reinterpret_cast<BYTE*>(&answ), sizeof(int));
        if (retCode != ERROR_SUCCESS)
        {
            result = false;
            goto Exit;
        }
    }

    result = true;

Exit:
    if (hCurrentKey != nullptr)
    {
        CloseHandle(hCurrentKey);
    }

    return result;
}

SandboxAnalyzerState getAnswer(ThreadContext* context, const char *filepath,
    std::unique_lock<std::mutex>& lk, int *pIsMalware)
{
    long statusCode = uploadFile(context->pCurl, filepath, context->netAddress.c_str());

    if (statusCode != 200)
    {
        return SandboxAnalyzerState::analyseIsFailed;
    }

    /* 1 means that FILE_ALREADY_IN_PROCESS on server */
    int analyseCode = 1;
    while (analyseCode == 1)
    {
        context->cv.wait_for(lk, 1s);
        if (context->ready == ThreadContext::ThreadState::threadToExit)
        {
            return SandboxAnalyzerState::analyseIsFailed;
        }

        analyseCode = 0;
        statusCode = sendGetRequest(context->pCurl, context->netAddress.c_str(), &analyseCode);
        if (statusCode != 200)
        {
            return SandboxAnalyzerState::analyseIsFailed;
        }
    }

    *pIsMalware = (analyseCode >> 1);
    return SandboxAnalyzerState::analyseIsEnded;
}

bool processAnalyseTask(HKEY hParentKey, const char* pKeyName,
    ThreadContext* context, std::unique_lock<std::mutex>& lk)
{
    bool result = false;

    char path[MAX_PATH];
    DWORD pathSize = MAX_PATH;
    int iAnswer;
    DWORD iAnswerSize = sizeof(int);

    LSTATUS retCode = 0;
    uint32_t status = static_cast<int>(SandboxAnalyzerState::analyseIsRunning);
    HKEY hCurrentKey = nullptr;
    SandboxAnalyzerState state;

    /* Get path value */
    retCode = RegGetValueA(hParentKey, pKeyName, AV_SUBKEY_PATH, RRF_RT_ANY,
        nullptr, reinterpret_cast<char*>(path), &pathSize);
    if (retCode != ERROR_SUCCESS)
    {
        result = true;
        goto Exit;
    }

    /* Set state to running */
    if (!setAnalyseStateAndAnsw(hParentKey, pKeyName, SandboxAnalyzerState::analyseIsRunning, 0))
    {
        result = true;
        goto Exit;
    }

    state = getAnswer(context, path, lk, &iAnswer);

    if (!setAnalyseStateAndAnsw(hParentKey, pKeyName, state, iAnswer))
    {
        result = true;
        goto Exit;
    }

    result = true;

Exit:
    return result;
}

bool processRegKeys(ThreadContext* context, std::unique_lock<std::mutex> &lk)
{
    bool result = false;

    LSTATUS retCode = ERROR_SUCCESS;
    HKEY hKey = nullptr;
    char achKey[MAX_KEY_LENGTH];   // buffer for subkey name

    retCode = RegCreateKeyExA(
        HKEY_LOCAL_MACHINE,                   // Handle of the parent registry key.
        AV_SANDBOX_REG_KEY,                   // Name of the new key to open or create.
        0,                                    // Reserved, pass 0.
        nullptr,                              // The user-defined class type of this key.
        0,                                    // Flags controlling the key creation
        KEY_ALL_ACCESS,                       // Access level desired.
        nullptr,                              // Security attributes for the key.
        &hKey,                                // Destination for the resulting handle.
        nullptr                               // Receives REG_CREATED_NEW_KEY or REG_OPENED_EXISTING_KEY.
    );
    if (retCode != ERROR_SUCCESS)
    {
        result = false;
        goto Exit;
    }

    for (uint32_t keyId = 0; retCode != ERROR_NO_MORE_ITEMS; keyId += 1)
    {
        DWORD cbKey = MAX_KEY_LENGTH;
        retCode = RegEnumKeyExA(
            hKey,
            keyId,
            reinterpret_cast<char*>(&achKey),
            &cbKey,
            nullptr,
            nullptr,
            nullptr,
            nullptr
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        /* Get state value */
        DWORD intValueSize = sizeof(uint32_t);
        uint32_t iStateValue;
        retCode = RegGetValueA(
            hKey,
            achKey,
            AV_SUBKEY_STATE,
            RRF_RT_ANY,
            nullptr,
            reinterpret_cast<char*>(&iStateValue),
            &intValueSize
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        if (iStateValue == static_cast<int>(SandboxAnalyzerState::analyseIsEnded))
        {
            continue;
        }

        processAnalyseTask(hKey, achKey, context, lk);
    }
    result = true;

Exit:
    if (hKey != nullptr)
    {
        CloseHandle(hKey);
        hKey = nullptr;
    }

    return result;
}

void threadWork(ThreadContext *context)
{
    std::unique_lock<std::mutex> lk(context->m);
    while (true)
    {
        processRegKeys(context, lk);

        if (context->ready == ThreadContext::ThreadState::threadToExit)
        {
            break;
        }

        context->ready = ThreadContext::ThreadState::waitForWork;
        while (context->ready == ThreadContext::ThreadState::waitForWork)
        {
            context->cv.wait(lk);
        }

        if (context->ready == ThreadContext::ThreadState::threadToExit)
        {
            break;
        }
    }
}
