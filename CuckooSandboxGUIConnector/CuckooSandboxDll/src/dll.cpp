#include <iostream>

#include "include/dll.h"
#include "include/sandboxAnalyzerContext.h"

AVHMODULE installModule()
{
    std::cout << "Dll: start" << std::endl;

    SandboxAnalyzerContext* pCtx = SandboxAnalyzerContext::build();

    if (pCtx == nullptr)
    {
        std::cout << "Dll: pCtx is nullptr" << std::endl;
        return nullptr;
    }

    std::cout << "Start to work" << std::endl;

    if (!pCtx->startWork(threadWork))
    {
        delete pCtx;
        return nullptr;
    }

    return pCtx;
}

unsigned int uninstallModule(AVHMODULE hModule)
{
    if (hModule != nullptr)
    {
        delete reinterpret_cast<SandboxAnalyzerContext *>(hModule);
    }

    return AV_STATUS_SUCCESS;
}

unsigned int updateModuleData(AVHMODULE hModule)
{
    if (hModule == nullptr)
    {
        return AV_STATUS_FAIL;
    }

    SandboxAnalyzerContext* pCtx = reinterpret_cast<SandboxAnalyzerContext *>(hModule);
    pCtx->doWorkAgain();

    return AV_STATUS_SUCCESS;
}
