#ifndef __SANDBOX_ANALYZER_CONTEXT_H__
#define __SANDBOX_ANALYZER_CONTEXT_H__

#include <memory>
#include <thread>

#include "threadWork.h"

typedef void (*func_threadWork)(ThreadContext *);

class SandboxAnalyzerContext
{
public:
	static SandboxAnalyzerContext* build();
	~SandboxAnalyzerContext();
	
	bool startWork(func_threadWork funcThreadWork);
	bool stopWork();
	bool doWorkAgain();

private:
	SandboxAnalyzerContext(void* pInCurl, std::string& inNetAddress);
	std::string netAddress;
	void* pCurl;
	std::unique_ptr<std::thread> pWorkThread;
	std::unique_ptr<ThreadContext> pThreadContext;
};

#endif // !__SANDBOX_ANALYZER_CONTEXT_H__
