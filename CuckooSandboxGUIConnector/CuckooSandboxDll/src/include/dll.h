#ifndef DLL_H
#define DLL_H

#ifdef CUCKOOSANDBOXDLL_EXPORTS
#define CUCKOOSANDBOXDLL_API __declspec(dllexport)
#else
#define CUCKOOSANDBOXDLL_API __declspec(dllimport)
#endif // NETFILTERDLL_EXPORTS

#define AV_STATUS_SUCCESS 1
#define AV_STATUS_FAIL 0

typedef void* AVHMODULE;

/* Installation preparations for module work */
extern "C" CUCKOOSANDBOXDLL_API AVHMODULE installModule();

/* Release all module resources */
extern "C" CUCKOOSANDBOXDLL_API unsigned int uninstallModule(AVHMODULE hModule);

/* Refresh all configs and tasks for module */
extern "C" CUCKOOSANDBOXDLL_API unsigned int updateModuleData(AVHMODULE hModule);

#endif // !DLL_H
