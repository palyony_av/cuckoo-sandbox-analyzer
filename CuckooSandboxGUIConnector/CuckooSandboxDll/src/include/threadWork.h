#ifndef __THREAD_WORK_H__
#define __THREAD_WORK_H__

#include <mutex>
#include <condition_variable>

extern "C"
{
#include "../util/include/curlWrapper.h"
}

#define AV_SANDBOX_REG_KEY "SOFTWARE\\PalyonyAV\\CloudScan"

struct ThreadContext
{
	ThreadContext(void *pInCurl, std::string & inNetAddress)
		: pCurl(pInCurl), ready(ThreadState::startToWork), netAddress(inNetAddress)
	{}

	void* pCurl;
	std::condition_variable cv;
	std::mutex m;
	std::string netAddress;

	enum class ThreadState
	{
		waitForWork = 0,
		startToWork = 1,
		threadToExit = 2
	};
	volatile ThreadState ready;
};

void threadWork(ThreadContext * context);

#endif // !__THREAD_WORK_H__
