#include <Windows.h>

#include "include/sandboxAnalyzerContext.h"

#define AV_SANDBOX_NETADDR "netaddr"
#define MAX_KEY_LENGTH 255

SandboxAnalyzerContext* SandboxAnalyzerContext::build()
{
	LSTATUS retCode;
	char achValue[MAX_KEY_LENGTH] = { 0 };
	DWORD dwValue = MAX_KEY_LENGTH;

	retCode = RegGetValueA(
		HKEY_LOCAL_MACHINE,
		AV_SANDBOX_REG_KEY,
		AV_SANDBOX_NETADDR,
		RRF_RT_REG_SZ,
		nullptr,
		reinterpret_cast<char*>(&achValue),
		&dwValue
	);
	if (retCode != ERROR_SUCCESS)
	{
		return nullptr;
	}

	std::string netAddress = std::string(achValue);

	void* pCurl = initCurl();

	/* Check server status by sending http request */
	long status = sendGetRequest(pCurl, netAddress.c_str(), nullptr);
	if (pCurl == nullptr || status != 200)
	{
		return nullptr;
	}

	SandboxAnalyzerContext* pCtx = new SandboxAnalyzerContext(pCurl, netAddress);
	return pCtx;
}

SandboxAnalyzerContext::SandboxAnalyzerContext(void* pInCurl, std::string& inNetAddress)
	: pCurl(pInCurl), netAddress(inNetAddress)
{

}

SandboxAnalyzerContext::~SandboxAnalyzerContext()
{
	this->stopWork();
	uninitCurl(&this->pCurl);
}

bool SandboxAnalyzerContext::startWork(func_threadWork funcThreadWork)
{
	if (this->pWorkThread != nullptr)
	{
		/* The previous work must ended */
		return false;
	}

	this->pThreadContext.reset(new ThreadContext(this->pCurl, this->netAddress));
	this->pWorkThread.reset(new std::thread(funcThreadWork, this->pThreadContext.get()));

	if (this->pWorkThread == nullptr)
	{
		return false;
	}

	return true;
}

bool SandboxAnalyzerContext::stopWork()
{
	if (this->pWorkThread == nullptr)
	{
		return true;
	}

	std::unique_lock<std::mutex> lk(this->pThreadContext->m);
	this->pThreadContext->ready = ThreadContext::ThreadState::threadToExit;
	lk.unlock();
	this->pThreadContext->cv.notify_one();

	this->pWorkThread->join();
	this->pWorkThread.reset(nullptr);
	this->pThreadContext.reset(nullptr);

	return true;
}

bool SandboxAnalyzerContext::doWorkAgain()
{
	if (this->pThreadContext == nullptr || this->pWorkThread == nullptr)
	{
		return false;
	}

	std::unique_lock<std::mutex> lk(this->pThreadContext->m);
	this->pThreadContext->ready = ThreadContext::ThreadState::startToWork;
	lk.unlock();
	this->pThreadContext->cv.notify_one();

	return true;
}
