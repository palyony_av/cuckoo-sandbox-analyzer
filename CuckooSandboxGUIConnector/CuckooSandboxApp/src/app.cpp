#include <Windows.h>
#include <atomic>
#include <iostream>
#include <thread>

typedef void* AVHMODULE;

#define AV_STATUS_SUCCESS 1
#define AV_STATUS_FAIL 0

typedef AVHMODULE (*func_installModule)();
typedef unsigned int (*func_uninstallModule)(AVHMODULE);
typedef unsigned int (*func_updateModuleData)(AVHMODULE);

#define DLL_PATH "CuckooSandboxDll.dll"

int main()
{
	HINSTANCE hinstLib;
    func_installModule fInstallModule;
    func_uninstallModule fUninstallModule;
    func_updateModuleData fUpdateModuleData;
	hinstLib = LoadLibrary(TEXT(DLL_PATH));

    if (hinstLib == nullptr)
    {
        return 0;
    }

    fInstallModule = reinterpret_cast<func_installModule>(GetProcAddress(hinstLib, "installModule"));
    fUninstallModule = reinterpret_cast<func_uninstallModule>(GetProcAddress(hinstLib, "uninstallModule"));
    fUpdateModuleData = reinterpret_cast<func_updateModuleData>(GetProcAddress(hinstLib, "updateModuleData"));

    // If the function address is valid, call the function.
    if (fInstallModule == nullptr || fUninstallModule == nullptr
        || fUpdateModuleData == nullptr)
    {
        std::cout << "Could not load functions" << std::endl;
        FreeLibrary(hinstLib);
        return 0;
    }

    // Free the DLL module.
    std::cout << "Install module" << std::endl;
    AVHMODULE hModule = fInstallModule();
    if (hModule == nullptr)
    {
        std::cout << "Can not open module" << std::endl;
        FreeLibrary(hinstLib);
        return 0;
    }

    std::cout << "Press any key to update module data" << std::endl;
    getchar();

    fUpdateModuleData(hModule);
    std::cout << "The module data is updated" << std::endl;
    
    std::cout << "Press any key to uninstall module" << std::endl;
    getchar();
    fUninstallModule(hModule);

    FreeLibrary(hinstLib);

    return 0;
}
