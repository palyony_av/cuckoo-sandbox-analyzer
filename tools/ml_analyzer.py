import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.feature_selection import chi2
from sklearn.feature_selection import SelectKBest
from sklearn.model_selection import train_test_split
# from sklearn import svm
# from sklearn.neural_network import MLPClassifier


class MLAnalyzer(object):
    def __init__(self, primary_dataset: list, primary_answers: list):
        np_data = np.asarray(primary_dataset)
        np_answ = np.asarray(primary_answers)

        ### Method chi2
        # Calculate which values need to remove
        self.__selecter = SelectKBest(score_func=chi2, k=80)
        self.__selecter.fit(np_data, np_answ)
        np_data = self.__selecter.transform(np_data)

        ### Create classifier
        # self.__classifier = svm.SVC(gamma='auto', random_state=0)
        # self.__classifier = MLPClassifier(hidden_layer_sizes=(140, 90, 60, 40), random_state=0)
        self.__classifier = RandomForestClassifier(n_jobs=2, random_state=0)
        self.__classifier.fit(np_data, np_answ)

    def predict_is_malw(self, entity_data: list) -> bool:
        entity_data_lst = [entity_data]
        np_entity_data = np.asarray(entity_data_lst)

        # Apply the method Chi2
        np_entity_data = self.__selecter.transform(np_entity_data)

        if self.__classifier.predict(np_entity_data)[0] == 1:
            return True
        else:
            return False
