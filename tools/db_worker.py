import sqlite3

from . import api_funcs


class DBWorker(object):
    def __init__(self, db_filename: str):
        self.__conn = sqlite3.connect(db_filename)
        self.__curs = self.__conn.cursor()
        self.__curs.execute(
            "SELECT name FROM sqlite_master WHERE type = 'table' AND name = 'cuckoo_worker'"
        )
        if self.__curs.fetchone() is None:
            self.__curs.execute(api_funcs.DB_CREATE_TABLE_QUERY)
            self.__conn.commit()

    def insert_list(self, data: list):
        insert_query = 'INSERT INTO cuckoo_worker (' + api_funcs.DB_COLUMNS + ') VALUES (' + ', '.join(
            map(
                lambda x: str(x),
                data
            )
        ) + ')'
        self.__curs.execute(insert_query)
        self.__conn.commit()

    def get_list_of_list(self) -> list:
        result = list()
        for row in self.__conn.execute("SELECT * FROM cuckoo_worker"):
            row_list = list()
            # Start from the second element because the first element is number only
            # It used to make uniqueness for every row
            for elem in row[1:]:
                row_list.append(elem)

            result.append(row_list)
        
        return result
