import argparse
import csv

from tools import db_worker


def convert(csv_file: str, csv_delimiter: str, db_file: str):
    result_lst = list()

    with open(csv_file, 'r') as file:
        reader = csv.reader(file, delimiter=csv_delimiter)
        for line in reader:
            result_lst.append(line)

    db = db_worker.DBWorker(db_file)
    for lst in result_lst:
        db.insert_list(lst)


parser = argparse.ArgumentParser(
    description='Convert Cuckoo csv results to SQLite DB')
parser.add_argument('--csv_file', type=str,
    help='Csv file with Cuckoo reports data')
parser.add_argument('--csv_delimiter', type=str, default=';',
    help='Delimiter of Csv file')
parser.add_argument('--db_file', type=str,
    help='SQLite DB file')
args = parser.parse_args()

if args.csv_file is not None and args.db_file is not None:
    convert(args.csv_file, args.csv_delimiter, args.db_file)
else:
    parser.print_help()
