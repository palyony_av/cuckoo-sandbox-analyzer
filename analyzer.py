from tools import db_worker
from tools import ml_analyzer


def build_analyzer(db_benign_path: str, db_malw_path: str) -> ml_analyzer.MLAnalyzer:
    db_benign = db_worker.DBWorker(db_benign_path)
    db_malw = db_worker.DBWorker(db_malw_path)
    data_benign = db_benign.get_list_of_list()
    data_malw = db_malw.get_list_of_list()
    
    data_lst = data_benign + data_malw
    answ_lst = [0 for _ in range(len(data_benign))] + [1 for _ in range(len(data_malw))]
    
    analyzer = ml_analyzer.MLAnalyzer(data_lst, answ_lst)
    return analyzer


# db_benign = db_worker.DBWorker('benign_worker.db')
# db_malw = db_worker.DBWorker('malw_worker.db')
# analyzer = build_analyzer(db_benign, db_malw)
# test_data = [0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,9,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
# print(analyzer.predict_is_malw(test_data))
