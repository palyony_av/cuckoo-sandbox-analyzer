import argparse
import csv
import os
import time

from tools import api_funcs
from tools import cuckoo_worker
from tools import db_worker


def analyze_dir(dir_path: str, db_filename: str, cuckoo_host: str,
                cuckoo_port: str, cuckoo_token: str) -> bool:
    if os.path.isdir(dir_path) == False:
        return False

    db = db_worker.DBWorker(db_filename)

    for rootdir, __dirnames, filenames in os.walk(dir_path):
        for filename in filenames:
            print('Start to analyze {}'.format(filename))
            signs = analyze_by_path(rootdir + os.path.sep + filename, cuckoo_host,
                    cuckoo_port, cuckoo_token)
            db.insert_list(signs)
            print('Analyze is done')

    return True


def wait_for_report(c_worker: cuckoo_worker.CuckooWorker, task_id: str) -> dict:
    task_info = c_worker.view_task_result(task_id)
    while task_info['task']['status'] != 'reported':
        time.sleep(5)
        task_info = c_worker.view_task_result(task_id)

    return c_worker.get_task_report(task_id)


def process_report(report: dict) -> list:
    detect_api_processes = report['behavior']['apistats']
    detect_api_funcs = dict()

    # Get count of API ocurrences for different processes
    for process_id in detect_api_processes.keys():
        for api_key_name in detect_api_processes[process_id].keys():
            if api_key_name in detect_api_funcs.keys():
                detect_api_funcs[api_key_name] += \
                    detect_api_processes[process_id][api_key_name]
            else:
                detect_api_funcs[api_key_name] = \
                    detect_api_processes[process_id][api_key_name]

    result = list()

    # Check if detected functions in my API list
    for func_name in api_funcs.APIS:
        counter = 0
        if func_name in detect_api_funcs.keys():
            counter = detect_api_funcs[func_name]
        
        result.append(counter)
    
    return result


def analyze_file(filedata: bytes, cuckoo_host: str, cuckoo_port: str,
                 cuckoo_token: str) -> list:
    c_worker = cuckoo_worker.CuckooWorker(cuckoo_host, cuckoo_port,
                                          cuckoo_token)
    task_id = c_worker.analyze_file(filedata)
    report = wait_for_report(c_worker, task_id)
    c_worker.delete_analyze_info(task_id)
    
    return process_report(report)


def analyze_by_path(filepath: str, cuckoo_host: str, cuckoo_port: str,
                    cuckoo_token: str) -> list:
    c_worker = cuckoo_worker.CuckooWorker(cuckoo_host, cuckoo_port,
                                          cuckoo_token)
    task_id = c_worker.analyze_file_by_path(filepath, filepath)
    report = wait_for_report(c_worker, task_id)
    c_worker.delete_analyze_info(task_id)
    
    return process_report(report)


# parser = argparse.ArgumentParser(
#     description='By using Cuckoo get API signature of executable file')
# parser.add_argument('--host', type=str, default='192.168.40.128',
#     help='Cuckoo VM Ip address')
# parser.add_argument('--port', type=str, default='8090',
#     help='Cuckoo VM port address')
# parser.add_argument('--token', type=str, default='eC7elTsNrvd2WmHSSAQTbQ',
#     help='Cuckoo VM bearer token')
# parser.add_argument('--dir', type=str,
#     help='Directory with executable files')
# parser.add_argument('--db', type=str, default='cuckoo_worker.db',
#     help='DB filename')
# args = parser.parse_args()

# if args.dir is not None:
#     analyze_dir(args.dir, args.db, args.host, args.port, args.token)
# else:
#     parser.print_help()
