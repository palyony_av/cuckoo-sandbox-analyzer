import argparse
import http.server
import threading

import api_server_state
import inveestigator
import analyzer


class ThreadCuckooProcessor(threading.Thread):
    def __init__(self, filedata: bytes, ml_analyzer, cuckoo_host: str,
                 cuckoo_port: str, cuckoo_token: str, *args, **kwargs):
        super(ThreadCuckooProcessor, self).__init__(*args, **kwargs)
        self.__filedata = filedata
        self.__ml_analyzer = ml_analyzer
        self.__cuckoo_host = cuckoo_host
        self.__cuckoo_port = cuckoo_port
        self.__cuckoo_token = cuckoo_token
        self.__return = None
    
    def run(self):
        signs_vector = inveestigator.analyze_file(
            self.__filedata,
            self.__cuckoo_host,
            self.__cuckoo_port,
            self.__cuckoo_token
        )
        self.__return = self.__ml_analyzer.predict_is_malw(signs_vector)

    def join(self):
        threading.Thread.join(self)
        return self.__return


class ApiServerContext(object):
    def __init__(self, ml_analyzer, cuckoo_host, cuckoo_port, cuckoo_token):
        self.__ml_analyzer = ml_analyzer
        self.__cuckoo_host = cuckoo_host
        self.__cuckoo_port = cuckoo_port
        self.__cuckoo_token = cuckoo_token
        self.__thread = None
        self.__thread_result = None

    def is_thread_ended(self) -> bool:
        if self.__thread is None:
            return True
        
        if not self.__thread.is_alive():
            self.__thread_result = self.__thread.join()
            self.__thread = None
            return True
        else:
            return False

    def run_thread(self, filedata: bytes):
        if filedata is None or len(filedata) == 0:
            return

        self.__thread = ThreadCuckooProcessor(
            filedata,
            self.__ml_analyzer,
            self.__cuckoo_host,
            self.__cuckoo_port,
            self.__cuckoo_token
        )
        self.__thread.start()
    
    def get_result_sign(self) -> int or None:
        if self.is_thread_ended():
            return self.__thread_result
        else:
            return None


class HttpReuqestHandler(http.server.BaseHTTPRequestHandler):
    def _set_headers(self, error_code: int = 200, content_type = "text/html"):
        self.send_response(error_code)
        # self.send_header("Content-type", "application/x-www-form-urlencoded")
        self.send_header("Content-type", content_type)
        self.end_headers()
    
    def do_GET(self):
        self._set_headers(200)

        is_malware = self.server.context.get_result_sign()
        if is_malware is None:
            self.wfile.write(api_server_state.FILE_ALREADY_IN_PROCESS)
        else:
            if not is_malware:
                self.wfile.write(api_server_state.RESULT_BENIGN)
            else:
                self.wfile.write(api_server_state.RESULT_MALWARE)

    def do_PUT(self):
        if not self.server.context.is_thread_ended():
            self._set_headers(400)
            self.wfile.write(api_server_state.FILE_ALREADY_IN_PROCESS)
            return
        
        self._set_headers(200)
        content_len = int(self.headers.get('content-length', 0))
        put_body = self.rfile.read(content_len)
        self.server.context.run_thread(put_body)

class MyHTTPServer(http.server.HTTPServer):
    def __init__(self, context: ApiServerContext, *args, **kwargs):
        self.context = context
        super().__init__(*args, **kwargs)


parser = argparse.ArgumentParser(
    description='By using Cuckoo get API signature of executable file')
parser.add_argument('--cuckoo_host', type=str, default='192.168.40.128',
    help='Cuckoo VM Ip address')
parser.add_argument('--cuckoo_port', type=str, default='8090',
    help='Cuckoo VM port address')
parser.add_argument('--cuckoo_token', type=str, default='eC7elTsNrvd2WmHSSAQTbQ',
    help='Cuckoo VM bearer token')
parser.add_argument('--db_benign', type=str, default='benign_worker.db',
    help='Benign DB filename')
parser.add_argument('--db_malw', type=str, default='malw_worker.db',
    help='Malware DB filename')
parser.add_argument('--bind', type=str, default='0.0.0.0',
    help='Ip address to listen')
parser.add_argument('--port', type=int, default=8080,
    help='Port to listen')
args = parser.parse_args()

ml_analyzer = analyzer.build_analyzer(args.db_benign, args.db_malw)
context = ApiServerContext(ml_analyzer, args.cuckoo_host, args.cuckoo_port, args.cuckoo_token)

with MyHTTPServer(context, (args.bind, args.port), HttpReuqestHandler) as httpd:
    print("serving at port", args.port)
    httpd.serve_forever()
